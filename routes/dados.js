var express = require("express");
var router = express.Router();

const jwt = require('jsonwebtoken')

var publicKey = process.env.PUBLIC_KEY;

console.log(process.env.PUBLIC_KEY)

router.get("/:num", function (req, res, next) {
  const bearerHeader = req.headers['authorization'];
  const token = bearerHeader.split(" ")[1]
  var verifyOptions = {
        algorithm:"RS256"
  }
  if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
  }

   const verifiacion = jwt.verify(token,publicKey,verifyOptions,function(err, verifiacion){
        if(err){
            res.status(400).send({message: "Token invalido"})
        }else{
          if (isNaN(req.params.num)) {
            res.sendStatus(400);
          }


          var output = { dados: [] };
          for (let i = 0; i < req.params.num; i++) {
            output.dados.push(Math.floor(Math.random() * 6) + 1);
          }
          res.status(200).send(output);
        }
    })
  

});

module.exports = router;
