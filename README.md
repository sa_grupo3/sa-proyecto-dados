# Microservicio Dados

Microservicio que simula una tirada indicando el numero de dados a utilizar.

## Instalación

Utilizar el administrador de paquetes [npm](https://www.npmjs.com) para instalar las dependencias.

```bash
npm install
```

## Uso

### Tirar Dados

`GET /tirar/{cantidad}`

curl -i -H 'Accept: application/json' http://localhost:3000/tirar/3

### Respuesta

    HTTP/1.1 200 OK
    Date: Thu, 29 Oct 2020 12:36:30 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: W/"11-/7jGa7JrZgdGWNMafVESBNGfxfc"

    { "dados": [6, 4, 3] }
